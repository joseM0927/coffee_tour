'use strict';

import Helpers from './helpers.js';
import Geolocalizacion from './geolocalizacion.js';
import Modal from './modal.js';

document.addEventListener('DOMContentLoaded', async event => {

    let sidebBar = document.querySelector('#sidebarCollapse');
    sidebBar.addEventListener('click', () => {
        document.querySelector('#sidebar').classList.toggle('active');
    })

    // lo referente a geolocalización
    // crear el objeto de tipo Geo
    // desplegar el mapa y los puntos
    let geo= await Geolocalizacion.crear()
    geo.desplegar()


    document.querySelector('#contactenos').addEventListener('click', e => {
        cargarContactenos()
    })
    document.querySelector('#nosotros').addEventListener('click', e => {
        cargarNosotros()
        
    })
    document.querySelector('#paisajeCafetero1').addEventListener('click', e => {
        cargarPaisajeCafetero(1)
        
    })
    document.querySelector('#paisajeCafetero2').addEventListener('click', e => {
        cargarPaisajeCafetero(2)
    })
    document.querySelector('#galeria').addEventListener('click', e => {
        cargarGaleria(geo.setImages())
    })


});


let cargarContactenos= function() {
    let idModal= 'contactenos';
    let modalHeader='Contactenos';
    let modalBody=`
    <form class="mx-auto" style="max-width: 45rem;">
        <div class="row mb-2">
            <div class="col-sm mb-1">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre">
            </div>
            <div class="col-sm mb-1">
            <label for="apellido" class="form-label">Apellido</label>
            <input type="text" class="form-control" id="apellido">
            </div>
        </div>
        <div class="mb-2">
            <label for="correo" class="form-label">Email</label>
            <input type="email" class="form-control" id="correo">
        </div>
        <div class="row mb-2">
            <div class="col-sm mb-1">
              <label for="departamento" class="form-label">Departamento</label>
              <select id="departamento" class="form-select" aria-label="Default select example">
                <option selected>Departamento</option>
                <option value="1">Antioquia</option>
                <option value="2">Cundinamarca</option>
                <option value="3">Caldas</option>
                <option value="4">Atlantico</option>
                <option value="5">Risaralda</option>
              </select>
            </div>
            <div class="col-sm mb-1">
              <label for="ciudad" class="form-label">Ciudad</label>
              <select id="ciudad" class="form-select" aria-label="Default select example">
                <option selected>Ciudad</option>
                <option value="1">Medellin</option>
                <option value="2">Bogota</option>
                <option value="3">Manizales</option>
                <option value="4">Barranquilla</option>
                <option value="5">Pereira</option>
              </select>
            </div>
            <div class="col-sm mb-1">
                <label for="direccion" class="form-label">Direccion</label>
                <input type="text" class="form-control" id="direccion">
            </div>
        </div>
        <div class="mb-2 border-top">
            <label for="mensaje" class="form-label mt-2">Escriba su mensaje</label>
            <textarea class="form-control" id="mensaje" rows="3"></textarea>
        </div>
        <div class="mb-2 form-check d-flex flex-row">
            <input type="checkbox" class="form-check-input mr-2" id="contactenme">
            <label class="form-check-label" for="contactenme">Quiero que me contacten</label>
        </div>
        <div class="d-flex flex-row justify-center my-1">
            <button type="submit" class="btn btn-primary btn-rounded ml-auto mr-1">Enviar</button>
            <button type="reset" class="btn btn-secondary btn-rounded mr-auto ml-1">Limpiar</button>
        </div>
    </form>   
    `;

    Modal.cargarModal(idModal, modalBody, modalHeader);

    document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', f => {
        Modal.cerrarModal(idModal)
    });

}

let cargarNosotros= function(){
    let idModal= 'nosotros'
    let modalHeader= `Coffee Tour`;
    let modalBody=`
        <div class="d-flex flex-column mx-auto my-1 justify-content-center" style="max-width: 50rem">
            
            
            
            <h4 class="mt-2">Mision</h4>
            <div>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium, 
                obcaecati neque facere enim vel ad nesciunt aliquid unde qui, 
                harum porro quisquam esse rerum iste optio sint? Hic, eligendi saepe.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium, 
                obcaecati neque facere enim vel ad nesciunt aliquid unde qui, 
                harum porro quisquam esse rerum iste optio sint? Hic, eligendi saepe.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium, 
                obcaecati neque facere enim vel ad nesciunt aliquid unde qui, 
                harum porro quisquam esse rerum iste optio sint? Hic, eligendi saepe. 
            </div>
            <div class"d-flex flex-row my-2">
                <img src="https://images.vexels.com/media/users/3/185520/isolated/preview/aae89bde5310bbc00a8f077465bfb22f-grano-de-caf--marr--n-by-vexels.png" class="rounded-lg" style="max-width: 4rem;">
                <img src="https://images.vexels.com/media/users/3/185520/isolated/preview/aae89bde5310bbc00a8f077465bfb22f-grano-de-caf--marr--n-by-vexels.png" class="rounded-lg" style="max-width: 4rem;">
                <img src="https://images.vexels.com/media/users/3/185520/isolated/preview/aae89bde5310bbc00a8f077465bfb22f-grano-de-caf--marr--n-by-vexels.png" class="rounded-lg" style="max-width: 4rem;">
            </div>
                <h4 class="mt-2">Vision</h4>
            <div>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium, 
                obcaecati neque facere enim vel ad nesciunt aliquid unde qui, 
                harum porro quisquam esse rerum iste optio sint? Hic, eligendi saepe.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium, 
                obcaecati neque facere enim vel ad nesciunt aliquid unde qui, 
                harum porro quisquam esse rerum iste optio sint? Hic, eligendi saepe.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium, 
                obcaecati neque facere enim vel ad nesciunt aliquid unde qui, 
                harum porro quisquam esse rerum iste optio sint? Hic, eligendi saepe. 
            </div>
        </div>
    `;

    Modal.cargarModal(idModal, modalBody, modalHeader);

    document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', f => {
        Modal.cerrarModal(idModal)
    })

}

let cargarPaisajeCafetero= function(opcion){
    let idModal='', modalBody=` `, modalHeader=``;
    if(opcion==1){
        idModal='pc1';
        modalHeader='Que es el Paisaje Cultural Cafetero?'
        modalBody=`
            <div class="mx-auto p-1" style="max-width:65rem;"> 
                <div class="flex flex-column ">
                    <p class="" style="max-width:">
                        El Paisaje Cultural Cafetero de Colombia (PCC) constituye un ejemplo 
                        de adaptación humana a condiciones geográficas difíciles sobre las 
                        que se desarrolló una caficultura montaña. Es un paisaje cultural en 
                        donde se encuentran elementos naturales, económicos y culturales con un 
                        alto grado de homogeneidad en la región, y constituye un caso excepcional 
                        en el mundo. En este paisaje se combinan el esfuerzo humano, familiar y 
                        generacional de los caficultores.
                    </p>
                    <div class="flex flex-row flex-wrap">
                        <img src="https://www.elespectador.com/resizer/3JWsb7IeP8rgrot1KRuSDtCPb7A=/657x0/cloudfront-us-east-1.images.arcpublishing.com/elespectador/LHJZYJJUR5E53FCWFJBJDAHSAI.jpg" class="mr-2 my-2" style="max-width:25rem; max-height:15rem;">
                        <img src="https://www.risaralda.gov.co/publicaciones/151780/aves-que-aman-el-paisaje-cultural-cafetero-se-toman-el-puente-helicoidal/info/gobrisaralda/media/pub/thumbs/thpub_700X400_151780.jpg" class="my-2" style="max-width:25rem; max-height:15rem;">
                    </div>
                </div>
                <div class="flex flex-column ">
                    <p class="" style="max-width:">
                        Aunados, estos esfuerzos han establecido un modelo de acción colectiva 
                        que ha permitido superar circunstancias económicas difíciles y sobrevivir 
                        en un paisaje agreste y aislado. De esta manera se ha desarrollado una 
                        caficultura basada en la pequeña propiedad, que ha demostrado su sostenibilidad 
                        en términos económicos, sociales y ambientales, y que ha posicionado su producto 
                        como uno de los mejores cafés del mundo. Este modelo social y económico ha 
                        configurado una región con un alto grado de unidad cultural, expresada en un 
                        patrimonio cultural material en el que se destacan las técnicas constructivas 
                        tanto de los asentamientos urbanos como de las viviendas cafeteras rurales, 
                        así como un patrimonio cultural inmaterial en el que se expresa el vínculo de 
                        la población con el cultivo por medio de fiestas, carnavales y celebraciones de 
                        la identidad paisa heredada de la colonización antioqueña, como rasgo único en el 
                        mundo creado por los habitantes de esta región.
                    </p>
                    <div class="flex flex-row flex-wrap">
                        <img src="https://www.elespectador.com/resizer/3JWsb7IeP8rgrot1KRuSDtCPb7A=/657x0/cloudfront-us-east-1.images.arcpublishing.com/elespectador/LHJZYJJUR5E53FCWFJBJDAHSAI.jpg" class="mr-2 my-2" style="max-width:25rem; max-height:15rem;">
                        <img src="https://www.risaralda.gov.co/publicaciones/151780/aves-que-aman-el-paisaje-cultural-cafetero-se-toman-el-puente-helicoidal/info/gobrisaralda/media/pub/thumbs/thpub_700X400_151780.jpg" class="my-2" style="max-width:25rem; max-height:15rem;">
                    </div>
                </div>
                <div class="mt-2">
                    <p>Obtenido de: Wikipedia, Gobernacion de risaralda</p>
                </div>
            </div>
        `;
    }
    else{
        idModal='pc2';
        modalHeader='Quienes lo componen?'
        modalBody=` 

        `;
    }

    Modal.cargarModal(idModal, modalBody, modalHeader);
    document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', f => {
        Modal.cerrarModal(idModal)
    })
}

let cargarGaleria= function(lista){
    let idModal='galeria';
    let modalHeader='Galeria';
    let modalBody=`
        <div>

        </div>
    `;

    Modal.cargarModal(idModal, modalBody, modalHeader);
    document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', f => {
        Modal.cerrarModal(idModal)
    })

}