import Helpers from './helpers.js';

let instancia;

export default class Geolocalizacion {

    #sitios;

    constructor() {
        this.#sitios = [];
        this.images= [];
    }

    static async crear() {
        instancia = new Geolocalizacion();
        let myJSON= await Helpers.leerJSON('./data/ubicaciones.geojson');
        instancia.#sitios= myJSON.features

        return instancia;

    }

    desplegar() {
    
        mapboxgl.accessToken = 'pk.eyJ1Ijoib2MzMjUiLCJhIjoiMTAxYjUxYmM4NWYxZjg2YjVhNTM3MjJmZDdhNTBjNTQifQ.d8Pqb9XknZ25-TcGqKUy3w';

        var bounds = [
            [-76.32202148437499, 3.908098881894123], // Southwest coordinates
            [-73.388671875, 6.0203850824560226] // Northeast coordinates
            
        ];

        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
            center: [-75.50010681152344, 5.0629021575810675], // starting position [lng, lat]
            zoom: 12, // starting zoom
            /*maxBounds: bounds*/
        });

        let userLocation= new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
                trackUserLocation: true
        });
        map.addControl(userLocation);

        let navControls = new mapboxgl.NavigationControl();
        map.addControl(navControls, 'top-right');

        let groups= [], cont=0, checksCont= 0;

        this.#sitios.forEach(element => {

            let marker = new mapboxgl.Marker()
                .setLngLat(element.geometry.coordinates)
                .setPopup(new mapboxgl.Popup().setHTML(`
                    <div class="d-flex flex-column m-0">
                        <h6 class="text-primary my-1">${element.properties.site}</h6>
                        <img src="${element.properties.img}" alt="" class="img-fluid mx-auto " style="max-width: 98%">
                        <p class="mt-1">${element.properties.description}</p>
                    <div>
                `)).addTo(map);
            
            document.getElementsByClassName('mapboxgl-marker')[cont].classList.add(`mapboxgl-marker-${element.properties.group}`);
            cont++;
            this.images.push(element.properties.img)

            if(!groups.includes(element.properties.group)){

                groups.push(element.properties.group)
                let addLi= `
                    <li class=" bg-white rounded-bottom px-2">
                        <input id="filtrar-${element.properties.group}" 
                        type="checkbox" checked> ${element.properties.group}
                    </li>
                `;
                checksCont+=1;

                document.querySelector('#filtro-opciones').insertAdjacentHTML('beforeend', addLi)

                document.querySelector(`#filtrar-${element.properties.group}`).addEventListener('change', e=> {
                    if(!e.target.checked){
                        document.querySelector('#filtrar-todos').checked= false
                        checksCont -= 1;
                    }
                    else{
                        checksCont += 1;
                        if(checksCont == groups.length){
                            document.querySelector('#filtrar-todos').checked= true
                        }
                    }
                })

            }

        });

        console.log(groups);

        document.querySelector('#filtro-opciones').insertAdjacentHTML('beforeend', `
            <li class=" bg-white rounded-bottom p-2">
                <button id="btn-filtrar" type="button" class="btn btn-outline-warning btn-sm rounded">filtrar</button>
            </li>
        `),

        document.querySelector('#filtrar-todos').addEventListener('change', e => {
            
            if( e.target.checked){
                groups.forEach( element => {
                    if(!document.querySelector(`#filtrar-${element}`).checked){
                        document.querySelector(`#filtrar-${element}`).checked= true
                        checksCont+=1;
                        if(checksCont >= groups.length){
                            document.querySelector('#filtrar-todos').checked= true
                        }
                    }
                })
            }
            else{
                groups.forEach( element => {
                    if(document.querySelector(`#filtrar-${element}`).checked){
                        document.querySelector(`#filtrar-${element}`).checked= false
                        checksCont-=1;
                    }
                })
            }
        });

        document.querySelector('#btn-filtrar').addEventListener('click', e => {
            if(document.querySelector('#filtrar-todos').checked){
                groups.forEach(element => {
                    this.vistaMarcador(element, 'visible')
                })
            }
            else{
               groups.forEach(element => {
                   if(!document.querySelector(`#filtrar-${element}`).checked){
                        this.vistaMarcador(element, 'invisible')
                   }
                   else{
                    this.vistaMarcador(element, 'visible')
                   }
               })
            }
        })

        document.querySelector('#input-buscar').addEventListener('input', e=> {

            document.querySelector('#busqueda-resultados').innerHTML=' '
            const texto= Helpers.eliminarEspaciosString(document.querySelector('#input-buscar').value).toLowerCase()
            
            let activo= false

            this.#sitios.forEach( sitio => {

                let textoSite=Helpers.eliminarEspaciosString(sitio.properties.site).toLowerCase();
                
                if(textoSite.includes(texto)){
                    activo= true
                    let resultado= `
                        <a id="s-${textoSite}" href="#" class="text-dark border-bottom" >${sitio.properties.site}</a>
                    `;

                    document.querySelector('#busqueda-resultados').insertAdjacentHTML('beforeend', resultado)
                    document.querySelector(`#s-${textoSite}`).addEventListener('click', e => {
                        
                        map.flyTo({
                            center: sitio.geometry.coordinates,
                            zoom: 15
                        })
                    })
                }
            })

            if(!activo){
                document.querySelector('#busqueda-resultados').insertAdjacentHTML('beforeend', `
                    <p>Sin resultados...</p>
                `)  
            }
            
        })
    }


    vistaMarcador(id, visibilidad){
        
        let lista= document.getElementsByClassName(`mapboxgl-marker-${id}`);

        if(visibilidad == 'visible'){
            lista.forEach(element => {
                if(element.classList.contains('invisible')){
                    element.classList.remove('invisible')
                }
            })
        }
        else{
            lista.forEach(element => {
                element.classList.add(`${visibilidad}`)
            })
        } 
    }

    setImages(){
        return this.images;
    }

}