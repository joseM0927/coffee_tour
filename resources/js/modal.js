export default class Modal {

    //
    static async cargarModal(id, modalBody=' ', modalHeader='', modalFooter='', modalClass=''){
        
        let myModal= `
            <div id="modal-${id}" class=" border rounded-lg text-center mx-auto mt-4 p-2 border-dark" style="max-width: 75rem; background-color: #ffffff;">
                <div class="mb-1 d-flex flex-row justify-content-between">
                    <i class="fas fa-times fa-lg invisible"></i>

                    <h4 class="text-dark font-bold my-1">${modalHeader}</h4>

                    <button type="button" id="btn-cerrar-modal-${id}" 
                    class="btn btn-outline-dark  rounded mr-1 my-1 p-1">
                        <i class="fas fa-times fa-lg"></i>
                    </button>
                </div>

                <div class="w-100 px-1" style="height: 1px; background-color: #6e819c;"></div>

                <div class="myScroll items-center my-2" style="overflow-y: scroll; max-height: 80vh;">
                    ${modalBody}
                </div>

                <div class="mt-1">
                    ${modalFooter}
                    
                <div>
            </div>
                
        `;

        document.querySelector(`#modal-container`).insertAdjacentHTML('afterbegin', myModal);
        document.querySelector(`#modal-container`).classList.remove('invisible');
        document.querySelector('#principal').classList.add('overflow-hidden');

    }

    static async cerrarModal(id){
        document.querySelector(`#modal-container`).removeChild(document.querySelector(`#modal-${id}`));
        document.querySelector(`#modal-container`).classList.add('invisible');
        document.querySelector('#principal').classList.remove('overflow-hidden');
    }

    


}